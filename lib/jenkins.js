'use strict';

const axios = require('axios');
const _ = require('lodash');
const FormData = require('form-data');
const Jenkins = require('jenkins');
const fs = require('fs');
const path = require('path');

const config = require('../config');

const jenkinsScript = fs.readFileSync(path.join(__dirname, 'jenkinsScript.groovy'), 'utf8');

let jenkins;

function initJenkinsClient() {
  jenkins = Jenkins({
    baseUrl: `http://${ config.workbench.credentials.username }:${ config.workbench.credentials.password }@${ config.jenkins.host }:${ config.jenkins.port }`,
    promisify: true,
  });
}

async function setCredentials() {
  const scripts = ['rancher', 'bitbucket'].map(key => {
    const credentialID = config[key].id ? config[key].id : key;
    const credentials = config[key].credentials;
    const script = groovyScripts.setCreds({
      credentialID,
      ...credentials,
    });

    const form = new FormData();
    form.append('script', script);

    const reqObj = {
      url: config.jenkins.endpoints.scriptText(),
      method: 'POST',
      data: form,
      headers: form.getHeaders(),
      auth: config.workbench.credentials,
    };

    return axios(reqObj);
  });

  const results = await Promise.all(scripts);
  return results;
}

const groovyScripts = {
  setCreds: ({ credentialID, username, password}) => {
    return jenkinsScript + `
      changePassword("${ credentialID }", "${ username }", "${ password }")`;
  },
};

function getJobList() {
  jenkins.job.list()
    .then(data => {
        console.log('jobs', data);
    });
}

module.exports = {
  initJenkinsClient,
  setCredentials,
  getJobList,
};