'use strict';

const axios = require('axios');
const _ = require('lodash');

const { workbench, rancher: config } = require('../config');

// use Bearer token auth if jwt is defined, otherwise auth with wb creds
const rancherAuth = () => {
  return _.has(config, 'credentials.jwt') ?
  { headers: { Authorization: `Bearer ${ config.credentials.jwt }` }} :
  { data: { code: `${ workbench.credentials.username }:${ workbench.credentials.password }` }};
};

const rancherReq = ({
  method='GET',
  url,
}) => axios.request({ url, method, ...rancherAuth() });

function setCreds({ wb_user, wb_pass }) {
  creds.wb_user = wb_user;
  creds.wb_pass = wb_pass;
}

async function getRancherJWT() {
  const res = await rancherReq({ url: config.endpoints.token, method: 'POST' });
  return res.data.jwt;
}

async function getRancherStacks({ filters }) {
  const queryString = _.reduce(filters, (str, val, key) => `${str}&${key}=${val}`, '?') || '';
  const url = config.endpoints.stacks() + queryString;
  const res = await rancherReq({ url });
  return res.data.data;
}

async function getRancherStackNames() {
  const stacks = await getRancherStacks();
  const stackNames = _.reduce(stacks, (names, stack) => {
    names.push(stack.name);
    return names;
  }, []);

  return stackNames;
}

async function getServicesByStackID({ stackID }) {
  const url = `${ config.endpoints.stacks() }/${ stackID }/services`;
  const res = await rancherReq({ url });
  return res.data.data;
}

async function getJenkinsURL() {
  const jenkinsStack = await getRancherStacks({ filters: { name: 'jenkins' }});
  const jenkinsServices = await getServicesByStackID({ stackID: jenkinsStack[0].id });
  const exposedService = _.find(jenkinsServices, service => service.publicEndpoints);
  const { ipAddress: host, port } = exposedService.publicEndpoints[0];
  return { host, port };
}

async function generateAPIKey() {
  const res = await rancherReq({ url: config.endpoints.apiKey });
}

module.exports = {
  getJenkinsURL,
  setCreds,
  getRancherJWT,
};