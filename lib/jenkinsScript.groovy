import com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl

def findCredential = { credentialId ->
    def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
        com.cloudbees.plugins.credentials.common.StandardUsernameCredentials.class,
        Jenkins.instance
    )

    return creds.findResult { cred.id == credentialId ? cred : null }
}

def changePassword = { id, username, password ->
    def c = findCredential(id)

    if ( c ) {
        println "found credential"

        def credentials_store = Jenkins.instance.getExtensionList(
            'com.cloudbees.plugins.credentials.SystemCredentialsProvider'
        )[0].getStore()

        def result = credentials_store.updateCredentials(
            com.cloudbees.plugins.credentials.domains.Domain.global(),
            c,
            new UsernamePasswordCredentialsImpl(c.scope, c.id, c.description, username, password)
        )

        if (result) {
            println "credentials changed"
        } else {
            println "failed to change credentials"
        }
    } else {
        println "could not find credentials"
    }
}

