'use strict';

const _ = require('lodash');

const required = param => new Error(`Missing required parameter ${ param }.`);

function setConfig({ key, config }) {
  if (!this[key]) throw new Error('Invalid config key.');
  this[key] = { ...this[key], ...config };
};

const config = {
  workbench: {},
  rancher: {
    environment: required('environment'),
  },
  bitbucket: {
    id: 'bitbucket-username-password',
  },
  'docker-registry': {},
  jenkins: {
    host: required('host'),
    port: required('port'),
  },
  setConfig,
}

const credentials = { username: required('username'), password: required('password') };
_.forEach(config, (val, key) => {
  config[key].credentials = { ...credentials }
  config[key].setConfig = setConfig;
});

const rancherEndpoint = ({ endpoint = '' }) => () =>
  `http://rancher.prod.eu-west-1.owlabs.io/v2-beta/projects/${ config.rancher.environment }/${ endpoint }`;

const jenkinsEndpoint = ({ endpoint = '' }) => () =>
  `http://${ config.jenkins.host }:${ config.jenkins.port }/${ endpoint }`;

config.rancher.endpoints = {
  token: 'http://rancher.prod.eu-west-1.owlabs.io/v2-beta/token',
  stacks: rancherEndpoint({ endpoint: 'stack' }),
  apiKey: rancherEndpoint({ endpoint: 'apiKey' }),
};

config.jenkins.endpoints = {
  scriptText: jenkinsEndpoint({ endpoint: 'scriptText' }),
};

module.exports = config;