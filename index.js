#!/usr/bin/env node
'use strict';

const inquirer = require('inquirer');

const rancher = require('./lib/rancher');
const jenkins = require('./lib/jenkins');

const config = require('./config');

async function getWorkbenchCreds() {
  const wbCreds = await inquirer
    .prompt([
      {
        type: 'input',
        message: 'Workbench Username: ',
        name: 'username',
      },
      {
        type: 'password',
        message: 'Workbench password: ',
        mask: '',
        name: 'password',
      }
    ]);

  config.workbench.setConfig({ key: 'credentials', config: wbCreds });
}

async function getRancherEnv() {
  const rancherEnv = await inquirer
    .prompt([
      {
        type: 'input',
        message: 'Rancher Environment: ',
        name: 'environment',
      },
    ]);

  config.rancher.environment = rancherEnv.environment;
}

async function getRancherCreds() {
  const rancherAPIKey = await inquirer
    .prompt([
      {
        type: 'input',
        message: 'Rancher API Key Username: ',
        name: 'username',
      },
      {
        type: 'password',
        message: 'Rancher API Key Password: ',
        mask: '',
        name: 'password',
      },
    ]);

  config.rancher.setConfig({
    key: 'credentials',
    config: rancherAPIKey,
  });
}

async function getBitbucketCreds() {
  const bbCreds = await inquirer
    .prompt([{
        type: 'input',
        message: 'Bitbucket Username: ',
        name: 'username',
      },
      {
        type: 'password',
        message: 'Bitbucket App Password: ',
        mask: '',
        name: 'password',
      },
    ]);

  config.bitbucket.setConfig({
    key: 'credentials',
    config: bbCreds,
  });
}

async function getJenkinsConfig() {
  const jwt = await rancher.getRancherJWT();
  config.rancher.setConfig({ key: 'credentials', config: { jwt }})
  const jenkinsConf = await rancher.getJenkinsURL();
  return jenkinsConf;
}

async function doJenkinsStuff() {
  const credentialStatus = await jenkins.setCredentials();
  await jenkins.getJobList();
}

async function main() {
  await getWorkbenchCreds();
  await getRancherEnv();
  await getRancherCreds();
  await getBitbucketCreds();
  const jenkinsConf = await getJenkinsConfig();
  config.setConfig({ key: 'jenkins', config: jenkinsConf });
  jenkins.initJenkinsClient();
  doJenkinsStuff();
}

main();